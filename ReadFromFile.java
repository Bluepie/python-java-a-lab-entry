import java.io.BufferedReader;
import java.io.FileReader;

public class ReadFromFile{

	private static final String FILENAME="C:\\info.txt";

	public static void main(String [] args){
		BufferedReader reading =  new BufferedReader(new FileReader(FILENAME));
		String line = null;
		while(( line = reading.readLine()) != null){
			String curLine = line;
			System.out.println(line);
		}
	}

}